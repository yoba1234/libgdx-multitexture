package yoba.teststuff

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.FPSLogger
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.ModdedSpriteBatch

class BatchExample : ApplicationAdapter() {
    private lateinit var batch: ModdedSpriteBatch
    private lateinit var camera: OrthographicCamera

    private lateinit var yobaTexture1: Texture
    private lateinit var yobaTexture2: Texture

    private val fpsLogger = FPSLogger()

    override fun create() {
        batch = ModdedSpriteBatch()
        camera = OrthographicCamera()
        yobaTexture1 = Texture("yoba_transparent_im.zktx")
        yobaTexture1.setFilter(Texture.TextureFilter.MipMapLinearNearest, Texture.TextureFilter.MipMapLinearNearest)

        yobaTexture2 = Texture("yoba2_1k.zktx")
        yobaTexture2.setFilter(Texture.TextureFilter.MipMapLinearNearest, Texture.TextureFilter.MipMapLinearNearest)
    }

    override fun resize(width: Int, height: Int) {
        val m = Math.min(width, height).toFloat()
        val x = width / m
        val y = height / m

        camera.setToOrtho(false, x, y)
        batch.projectionMatrix = camera.combined
    }

    override fun render() {
        fpsLogger.log()

        Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 0f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        batch.use {
            draw(yobaTexture1, 0f, 0f, 0.5f, 0.5f)
            draw(yobaTexture2, 0.5f, 0.5f, 0.5f, 0.5f)
        }

        println(batch.renderCalls)
    }

    override fun dispose() {
        batch.dispose()
        yobaTexture1.dispose()
        yobaTexture2.dispose()
    }
}

inline fun <T : Batch, reified R> T.use(block: T.() -> R): R {
    this.begin()
    val result = block()
    this.end()
    return result
}