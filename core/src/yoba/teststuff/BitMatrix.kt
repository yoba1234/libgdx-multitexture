package yoba.teststuff

import java.util.*

class BitMatrix(val size: Int) {
    val bits = BitSet(size * size)
    fun set(x: Int, y: Int, value: Boolean) {
        bits.set(x + y * size, value)
    }

    fun get(x: Int, y: Int): Boolean {
        return bits.get(x + y * size)
    }
}