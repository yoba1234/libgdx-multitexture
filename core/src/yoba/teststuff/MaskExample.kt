package yoba.teststuff

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.PixmapTextureData
import com.badlogic.gdx.graphics.glutils.ShaderProgram

class MaskExample : ApplicationAdapter() {
    private lateinit var batch: SpriteBatch
    private lateinit var camera: OrthographicCamera

    private lateinit var maskPixmap: Pixmap
    private lateinit var maskTexture: Texture
    private lateinit var maskTextureData: PixmapTextureData
    private val maskImageBytes = ByteArray(gridSize * gridSize * 4)

    private lateinit var yobaTexture: Texture

    private val hiddenCells = BitMatrix(gridSize)

    init {
        hiddenCells.run {
            set(2, 2, true)
            set(2, 3, true)
            set(3, 3, true)
            set(4, 3, true)
            set(4, 4, true)
            set(3, 4, true)
        }
    }

    private val fpsLogger = FPSLogger()

    private lateinit var yobaShader: ShaderProgram

    private fun updateMaskTexture() {
        val pixels = maskPixmap.pixels
        pixels.rewind()

        for (i in maskImageBytes.indices) {
            val y: Int = i / (gridSize * 2) / 2
            val x: Int = i % (gridSize * 2) / 2

            maskImageBytes[i] = if (hiddenCells.get(x, y)) 0xff.toByte() else 0
        }

        pixels.put(maskImageBytes)
        pixels.rewind()
        maskTexture.load(maskTextureData)
    }

    override fun create() {
        batch = SpriteBatch()
        yobaShader = createShader()

        yobaShader.begin()
        yobaShader.setUniformi("u_texture2", 1)

        camera = OrthographicCamera()

        createMaskData()
        updateMaskTexture()

        yobaTexture = Texture("yoba_transparent_im.zktx")
        yobaTexture.setFilter(Texture.TextureFilter.MipMapLinearNearest, Texture.TextureFilter.MipMapLinearNearest)
        yobaTexture.bind(1)
        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0)
    }

    private fun createMaskData() {
        maskPixmap = Pixmap(gridSize * 2, gridSize * 2, Pixmap.Format.Alpha)
        maskTextureData = PixmapTextureData(maskPixmap, null, false, false)
        maskTexture = Texture(maskPixmap)
        maskTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
    }

    override fun dispose() {
        batch.dispose()
        maskPixmap.dispose()
        maskTexture.dispose()
    }

    override fun render() {
        fpsLogger.log()
        Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 0f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        batch.begin()
        batch.shader = yobaShader
        batch.draw(maskTexture, 0f, 0f, 1f, 1f)
        batch.end()
    }

    override fun resize(width: Int, height: Int) {
        val m = Math.min(width, height).toFloat()
        val x = width / m
        val y = height / m

        camera.setToOrtho(false, x, y)
        batch.projectionMatrix = camera.combined
    }

    companion object {
        private val gridSize = 8

        private fun createShader(): ShaderProgram {
            val vertexShader = """
attribute vec4 ${ShaderProgram.POSITION_ATTRIBUTE};
attribute vec4 ${ShaderProgram.COLOR_ATTRIBUTE};
attribute vec2 ${ShaderProgram.TEXCOORD_ATTRIBUTE}0;
uniform mat4 u_projTrans;
varying vec4 v_color;
varying vec2 v_texCoords;

void main() {
   v_color = ${ShaderProgram.COLOR_ATTRIBUTE};
   v_color.a = v_color.a * (255.0/254.0);
   v_texCoords = ${ShaderProgram.TEXCOORD_ATTRIBUTE}0;
   gl_Position = u_projTrans * ${ShaderProgram.POSITION_ATTRIBUTE};
}
"""
            val fragmentShader = """
#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;
uniform sampler2D u_texture;
uniform sampler2D u_texture2;

void main() {
  vec4 imageColor = texture2D(u_texture2, v_texCoords);
  imageColor.a = texture2D(u_texture, v_texCoords).a;
  gl_FragColor = v_color * imageColor;
}
"""

            val shader = ShaderProgram(vertexShader, fragmentShader)
            if (!shader.isCompiled)
                throw IllegalArgumentException("Error compiling shader: " + shader.log)
            return shader
        }
    }
}

