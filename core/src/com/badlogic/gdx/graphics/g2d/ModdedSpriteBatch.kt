/*******************************************************************************
 * Copyright 2011 See AUTHORS file.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.badlogic.gdx.graphics.g2d

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.Mesh.VertexDataType
import com.badlogic.gdx.graphics.VertexAttributes.Usage
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.Affine2
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.utils.NumberUtils

/**
 * Draws batched quads using indices.

 * @author mzechner
 * *
 * @author Nathan Sweet
 * *
 * @see Batch
 */
class ModdedSpriteBatch
/**
 * Constructs a new SpriteBatch. Sets the projection matrix to an orthographic projection with y-axis point upwards, x-axis
 * point to the right and the origin being in the bottom left corner of the screen. The projection will be pixel perfect with
 * respect to the current screen resolution.
 *
 *
 * The defaultShader specifies the shader to use. Note that the names for uniforms for this default shader are different than
 * the ones expect for shaders set with [.setShader]. See [.createDefaultShader].

 * @param size          The max number of sprites in a single batch.
 * *
 * @param defaultShader The default shader to use. This is not owned by the SpriteBatch and must be disposed separately.
 */
@JvmOverloads constructor(size: Int = 1000, defaultShader: ShaderProgram? = null) : Batch {

    private val mesh: Mesh

    internal val vertices: FloatArray
    internal var idx = 0
    internal var lastTexture: Texture? = null
    internal var invTexWidth = 0f
    internal var invTexHeight = 0f

    internal var drawing = false

    private val transformMatrix = Matrix4()
    private val projectionMatrix = Matrix4()
    private val combinedMatrix = Matrix4()

    private var blendingDisabled = false
    private var blendSrcFunc = GL20.GL_SRC_ALPHA
    private var blendDstFunc = GL20.GL_ONE_MINUS_SRC_ALPHA

    private val shader: ShaderProgram?
    private var customShader: ShaderProgram? = null
    private var ownsShader: Boolean = false

    internal var color = Color.WHITE.toFloatBits()
    private val tempColor = Color(1f, 1f, 1f, 1f)

    /**
     * Number of render calls since the last [.begin].
     */
    var renderCalls = 0

    /**
     * Number of rendering calls, ever. Will not be reset unless set manually.
     */
    var totalRenderCalls = 0

    /**
     * The maximum number of sprites rendered in one batch so far.
     */
    var maxSpritesInBatch = 0

    val indicesPerSprite = 6
    val verticesPerSprite = 4

    val maxSize = 32767 / indicesPerSprite - ((32767 / indicesPerSprite) % 3)

    init {
        if (size > maxSize) throw IllegalArgumentException("Can't have more than ${maxSize} sprites per batch: " + size)

        val vertexDataType = if (Gdx.gl30 != null) VertexDataType.VertexBufferObjectWithVAO else defaultVertexDataType

        val indicesCount = size * indicesPerSprite
        mesh = Mesh(vertexDataType, false,
                size * verticesPerSprite,
                indicesCount,
                VertexAttribute(Usage.Position, 2, ShaderProgram.POSITION_ATTRIBUTE),
                VertexAttribute(Usage.ColorPacked, 4, ShaderProgram.COLOR_ATTRIBUTE),
                VertexAttribute(Usage.TextureCoordinates, 2, ShaderProgram.TEXCOORD_ATTRIBUTE + "0"))

        projectionMatrix.setToOrtho2D(0f, 0f, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())

        vertices = FloatArray(size * Sprite.SPRITE_SIZE)

        val indices = ShortArray(indicesCount)
        var j = 0
        var i = 0
        while (i < indicesCount) {
            indices[i++] = j.toShort()
            indices[i++] = (j + 1).toShort()
            indices[i++] = (j + 2).toShort()
            indices[i++] = (j + 2).toShort()
            indices[i++] = (j + 3).toShort()
            indices[i++] = j.toShort()
            j += verticesPerSprite
        }
        mesh.setIndices(indices)

        if (defaultShader == null) {
            shader = createDefaultShader()
            ownsShader = true
        } else
            shader = defaultShader
    }

    override fun begin() {
        if (drawing) throw IllegalStateException("SpriteBatch.end must be called before begin.")
        renderCalls = 0

        Gdx.gl.glDepthMask(false)
        if (customShader != null)
            customShader!!.begin()
        else
            shader!!.begin()
        setupMatrices()

        drawing = true
    }

    override fun end() {
        if (!drawing) throw IllegalStateException("SpriteBatch.begin must be called before end.")
        if (idx > 0) flush()
        lastTexture = null
        drawing = false

        val gl = Gdx.gl
        gl.glDepthMask(true)
        if (isBlendingEnabled) gl.glDisable(GL20.GL_BLEND)

        if (customShader != null)
            customShader!!.end()
        else
            shader!!.end()
    }

    private fun switchTextureAndFlushIfNeeded(texture: Texture, vertices: FloatArray) {
        if (texture !== lastTexture)
            switchTexture(texture)
        else if (idx == vertices.size)
            flush()
    }

    private fun putValues(vertices: FloatArray, idx: Int, v1: Float, v2: Float, v3: Float, v4: Float, v5: Float): Int {
        vertices[idx] = v1
        vertices[idx + 1] = v2
        vertices[idx + 2] = v3
        vertices[idx + 3] = v4
        vertices[idx + 4] = v5
        return idx + 5
    }

    override fun setColor(tint: Color) {
        color = tint.toFloatBits()
    }

    override fun setColor(r: Float, g: Float, b: Float, a: Float) {
        val intBits = (255 * a).toInt() shl 24 or ((255 * b).toInt() shl 16) or ((255 * g).toInt() shl 8) or (255 * r).toInt()
        color = NumberUtils.intToFloatColor(intBits)
    }

    override fun setColor(color: Float) {
        this.color = color
    }

    override fun getColor(): Color {
        val intBits = NumberUtils.floatToIntColor(color)
        val color = tempColor
        color.r = (intBits and 0xff) / 255f
        color.g = (intBits.ushr(8) and 0xff) / 255f
        color.b = (intBits.ushr(16) and 0xff) / 255f
        color.a = (intBits.ushr(24) and 0xff) / 255f
        return color
    }

    override fun getPackedColor(): Float {
        return color
    }

    override fun draw(texture: Texture, x: Float, y: Float, originX: Float, originY: Float, width: Float, height: Float, scaleX: Float,
                      scaleY: Float, rotation: Float, srcX: Int, srcY: Int, srcWidth: Int, srcHeight: Int, flipX: Boolean, flipY: Boolean) {
        checkBegin()

        val vertices = this.vertices

        switchTextureAndFlushIfNeeded(texture, vertices)

        // bottom left and top right corner points relative to origin
        val worldOriginX = x + originX
        val worldOriginY = y + originY
        var fx = -originX
        var fy = -originY
        var fx2 = width - originX
        var fy2 = height - originY

        // scale
        if (scaleX != 1f || scaleY != 1f) {
            fx *= scaleX
            fy *= scaleY
            fx2 *= scaleX
            fy2 *= scaleY
        }

        // construct corner points, start from top left and go counter clockwise
        val p1x = fx
        val p1y = fy
        val p2x = fx
        val p2y = fy2
        val p3x = fx2
        val p3y = fy2
        val p4x = fx2
        val p4y = fy

        var x1: Float
        var y1: Float
        var x2: Float
        var y2: Float
        var x3: Float
        var y3: Float
        var x4: Float
        var y4: Float

        // rotate
        if (rotation != 0f) {
            val cos = MathUtils.cosDeg(rotation)
            val sin = MathUtils.sinDeg(rotation)

            x1 = cos * p1x - sin * p1y
            y1 = sin * p1x + cos * p1y

            x2 = cos * p2x - sin * p2y
            y2 = sin * p2x + cos * p2y

            x3 = cos * p3x - sin * p3y
            y3 = sin * p3x + cos * p3y

            x4 = x1 + (x3 - x2)
            y4 = y3 - (y2 - y1)
        } else {
            x1 = p1x
            y1 = p1y

            x2 = p2x
            y2 = p2y

            x3 = p3x
            y3 = p3y

            x4 = p4x
            y4 = p4y
        }

        x1 += worldOriginX
        y1 += worldOriginY
        x2 += worldOriginX
        y2 += worldOriginY
        x3 += worldOriginX
        y3 += worldOriginY
        x4 += worldOriginX
        y4 += worldOriginY

        var u = srcX * invTexWidth
        var v = (srcY + srcHeight) * invTexHeight
        var u2 = (srcX + srcWidth) * invTexWidth
        var v2 = srcY * invTexHeight

        if (flipX) {
            val tmp = u
            u = u2
            u2 = tmp
        }

        if (flipY) {
            val tmp = v
            v = v2
            v2 = tmp
        }

        val color = this.color
        var idx = this.idx

        idx = putValues(vertices, idx, x1, y1, color, u, v)
        idx = putValues(vertices, idx, x2, y2, color, u, v2)
        idx = putValues(vertices, idx, x3, y3, color, u2, v2)
        idx = putValues(vertices, idx, x4, y4, color, u2, v)

        this.idx = idx
    }

    override fun draw(texture: Texture, x: Float, y: Float, width: Float, height: Float, srcX: Int, srcY: Int, srcWidth: Int,
                      srcHeight: Int, flipX: Boolean, flipY: Boolean) {
        checkBegin()

        val vertices = this.vertices

        switchTextureAndFlushIfNeeded(texture, vertices)

        var u = srcX * invTexWidth
        var v = (srcY + srcHeight) * invTexHeight
        var u2 = (srcX + srcWidth) * invTexWidth
        var v2 = srcY * invTexHeight
        val fx2 = x + width
        val fy2 = y + height

        if (flipX) {
            val tmp = u
            u = u2
            u2 = tmp
        }

        if (flipY) {
            val tmp = v
            v = v2
            v2 = tmp
        }

        val color = this.color
        var idx = this.idx

        idx = putValues(vertices, idx, x, y, color, u, v)
        idx = putValues(vertices, idx, x, fy2, color, u, v2)
        idx = putValues(vertices, idx, fx2, fy2, color, u2, v2)
        idx = putValues(vertices, idx, fx2, y, color, u2, v)

        this.idx = idx
    }

    override fun draw(texture: Texture, x: Float, y: Float, srcX: Int, srcY: Int, srcWidth: Int, srcHeight: Int) {
        checkBegin()

        val vertices = this.vertices

        switchTextureAndFlushIfNeeded(texture, vertices)

        val u = srcX * invTexWidth
        val v = (srcY + srcHeight) * invTexHeight
        val u2 = (srcX + srcWidth) * invTexWidth
        val v2 = srcY * invTexHeight
        val fx2 = x + srcWidth
        val fy2 = y + srcHeight

        val color = this.color
        var idx = this.idx

        idx = putValues(vertices, idx, x, y, color, u, v)
        idx = putValues(vertices, idx, x, fy2, color, u, v2)
        idx = putValues(vertices, idx, fx2, fy2, color, u2, v2)
        idx = putValues(vertices, idx, fx2, y, color, u2, v)

        this.idx = idx
    }

    override fun draw(texture: Texture, x: Float, y: Float, width: Float, height: Float, u: Float, v: Float, u2: Float, v2: Float) {
        checkBegin()

        val vertices = this.vertices

        switchTextureAndFlushIfNeeded(texture, vertices)

        val fx2 = x + width
        val fy2 = y + height

        val color = this.color
        var idx = this.idx

        idx = putValues(vertices, idx, x, y, color, u, v)
        idx = putValues(vertices, idx, x, fy2, color, u, v2)
        idx = putValues(vertices, idx, fx2, fy2, color, u2, v2)
        idx = putValues(vertices, idx, fx2, y, color, u2, v)

        this.idx = idx
    }

    override fun draw(texture: Texture, x: Float, y: Float) {
        draw(texture, x, y, texture.width.toFloat(), texture.height.toFloat())
    }

    override fun draw(texture: Texture, x: Float, y: Float, width: Float, height: Float) {
        checkBegin()

        val vertices = this.vertices

        switchTextureAndFlushIfNeeded(texture, vertices)

        val fx2 = x + width
        val fy2 = y + height
        val u = 0f
        val v = 1f
        val u2 = 1f
        val v2 = 0f

        val color = this.color
        var idx = this.idx

        idx = putValues(vertices, idx, x, y, color, u, v)
        idx = putValues(vertices, idx, x, fy2, color, u, v2)
        idx = putValues(vertices, idx, fx2, fy2, color, u2, v2)
        idx = putValues(vertices, idx, fx2, y, color, u2, v)

        this.idx = idx
    }

    override fun draw(texture: Texture, spriteVertices: FloatArray, offset: Int, count: Int) {
        TODO()
//        var offset = offset
//        var count = count
//        checkBegin()
//
//        val verticesLength = vertices.size
//        var remainingVertices = verticesLength
//        if (texture !== lastTexture)
//            switchTexture(texture)
//        else {
//            remainingVertices -= idx
//            if (remainingVertices == 0) {
//                flush()
//                remainingVertices = verticesLength
//            }
//        }
//        var copyCount = Math.min(remainingVertices, count)
//
//        System.arraycopy(spriteVertices, offset, vertices, idx, copyCount)
//        idx += copyCount
//        count -= copyCount
//        while (count > 0) {
//            offset += copyCount
//            flush()
//            copyCount = Math.min(verticesLength, count)
//            System.arraycopy(spriteVertices, offset, vertices, 0, copyCount)
//            idx += copyCount
//            count -= copyCount
//        }
    }

    override fun draw(region: TextureRegion, x: Float, y: Float) {
        draw(region, x, y, region.getRegionWidth().toFloat(), region.getRegionHeight().toFloat())
    }

    override fun draw(region: TextureRegion, x: Float, y: Float, width: Float, height: Float) {
        checkBegin()

        val vertices = this.vertices

        val texture = region.texture

        switchTextureAndFlushIfNeeded(texture, vertices)

        val fx2 = x + width
        val fy2 = y + height
        val u = region.u
        val v = region.v2
        val u2 = region.u2
        val v2 = region.v

        val color = this.color
        var idx = this.idx

        idx = putValues(vertices, idx, x, y, color, u, v)
        idx = putValues(vertices, idx, x, fy2, color, u, v2)
        idx = putValues(vertices, idx, fx2, fy2, color, u2, v2)
        idx = putValues(vertices, idx, fx2, y, color, u2, v)
        this.idx = idx
    }

    override fun draw(region: TextureRegion, x: Float, y: Float, originX: Float, originY: Float, width: Float, height: Float,
                      scaleX: Float, scaleY: Float, rotation: Float) {
        checkBegin()

        val vertices = this.vertices

        val texture = region.texture

        switchTextureAndFlushIfNeeded(texture, vertices)

        // bottom left and top right corner points relative to origin
        val worldOriginX = x + originX
        val worldOriginY = y + originY
        var fx = -originX
        var fy = -originY
        var fx2 = width - originX
        var fy2 = height - originY

        // scale
        if (scaleX != 1f || scaleY != 1f) {
            fx *= scaleX
            fy *= scaleY
            fx2 *= scaleX
            fy2 *= scaleY
        }

        // construct corner points, start from top left and go counter clockwise
        val p1x = fx
        val p1y = fy
        val p2x = fx
        val p2y = fy2
        val p3x = fx2
        val p3y = fy2
        val p4x = fx2
        val p4y = fy

        var x1: Float
        var y1: Float
        var x2: Float
        var y2: Float
        var x3: Float
        var y3: Float
        var x4: Float
        var y4: Float

        // rotate
        if (rotation != 0f) {
            val cos = MathUtils.cosDeg(rotation)
            val sin = MathUtils.sinDeg(rotation)

            x1 = cos * p1x - sin * p1y
            y1 = sin * p1x + cos * p1y

            x2 = cos * p2x - sin * p2y
            y2 = sin * p2x + cos * p2y

            x3 = cos * p3x - sin * p3y
            y3 = sin * p3x + cos * p3y

            x4 = x1 + (x3 - x2)
            y4 = y3 - (y2 - y1)
        } else {
            x1 = p1x
            y1 = p1y

            x2 = p2x
            y2 = p2y

            x3 = p3x
            y3 = p3y

            x4 = p4x
            y4 = p4y
        }

        x1 += worldOriginX
        y1 += worldOriginY
        x2 += worldOriginX
        y2 += worldOriginY
        x3 += worldOriginX
        y3 += worldOriginY
        x4 += worldOriginX
        y4 += worldOriginY

        val u = region.u
        val v = region.v2
        val u2 = region.u2
        val v2 = region.v

        val color = this.color
        var idx = this.idx
        idx = putValues(vertices, idx, x1, y1, color, u, v)
        idx = putValues(vertices, idx, x2, y2, color, u, v2)
        idx = putValues(vertices, idx, x3, y3, color, u2, v2)
        idx = putValues(vertices, idx, x4, y4, color, u2, v)
        this.idx = idx
    }

    override fun draw(region: TextureRegion, x: Float, y: Float, originX: Float, originY: Float, width: Float, height: Float,
                      scaleX: Float, scaleY: Float, rotation: Float, clockwise: Boolean) {
        checkBegin()

        val vertices = this.vertices

        val texture = region.texture
        switchTextureAndFlushIfNeeded(texture, vertices)

        // bottom left and top right corner points relative to origin
        val worldOriginX = x + originX
        val worldOriginY = y + originY
        var fx = -originX
        var fy = -originY
        var fx2 = width - originX
        var fy2 = height - originY

        // scale
        if (scaleX != 1f || scaleY != 1f) {
            fx *= scaleX
            fy *= scaleY
            fx2 *= scaleX
            fy2 *= scaleY
        }

        // construct corner points, start from top left and go counter clockwise
        val p1x = fx
        val p1y = fy
        val p2x = fx
        val p2y = fy2
        val p3x = fx2
        val p3y = fy2
        val p4x = fx2
        val p4y = fy

        var x1: Float
        var y1: Float
        var x2: Float
        var y2: Float
        var x3: Float
        var y3: Float
        var x4: Float
        var y4: Float

        // rotate
        if (rotation != 0f) {
            val cos = MathUtils.cosDeg(rotation)
            val sin = MathUtils.sinDeg(rotation)

            x1 = cos * p1x - sin * p1y
            y1 = sin * p1x + cos * p1y

            x2 = cos * p2x - sin * p2y
            y2 = sin * p2x + cos * p2y

            x3 = cos * p3x - sin * p3y
            y3 = sin * p3x + cos * p3y

            x4 = x1 + (x3 - x2)
            y4 = y3 - (y2 - y1)
        } else {
            x1 = p1x
            y1 = p1y

            x2 = p2x
            y2 = p2y

            x3 = p3x
            y3 = p3y

            x4 = p4x
            y4 = p4y
        }

        x1 += worldOriginX
        y1 += worldOriginY
        x2 += worldOriginX
        y2 += worldOriginY
        x3 += worldOriginX
        y3 += worldOriginY
        x4 += worldOriginX
        y4 += worldOriginY

        val u1: Float
        val v1: Float
        val u2: Float
        val v2: Float
        val u3: Float
        val v3: Float
        val u4: Float
        val v4: Float
        if (clockwise) {
            u1 = region.u2
            v1 = region.v2
            u2 = region.u
            v2 = region.v2
            u3 = region.u
            v3 = region.v
            u4 = region.u2
            v4 = region.v
        } else {
            u1 = region.u
            v1 = region.v
            u2 = region.u2
            v2 = region.v
            u3 = region.u2
            v3 = region.v2
            u4 = region.u
            v4 = region.v2
        }

        val color = this.color
        var idx = this.idx
        idx = putValues(vertices, idx, x1, y1, color, u1, v1)
        idx = putValues(vertices, idx, x2, y2, color, u2, v2)
        idx = putValues(vertices, idx, x3, y3, color, u3, v3)
        idx = putValues(vertices, idx, x4, y4, color, u4, v4)
        this.idx = idx
    }

    override fun draw(region: TextureRegion, width: Float, height: Float, transform: Affine2) {
        checkBegin()

        val vertices = this.vertices

        val texture = region.texture
        switchTextureAndFlushIfNeeded(texture, vertices)

        // construct corner points
        val x1 = transform.m02
        val y1 = transform.m12
        val x2 = transform.m01 * height + transform.m02
        val y2 = transform.m11 * height + transform.m12
        val x3 = transform.m00 * width + transform.m01 * height + transform.m02
        val y3 = transform.m10 * width + transform.m11 * height + transform.m12
        val x4 = transform.m00 * width + transform.m02
        val y4 = transform.m10 * width + transform.m12

        val u = region.u
        val v = region.v2
        val u2 = region.u2
        val v2 = region.v

        val color = this.color
        var idx = this.idx
        idx = putValues(vertices, idx, x1, y1, color, u, v)
        idx = putValues(vertices, idx, x2, y2, color, u, v2)
        idx = putValues(vertices, idx, x3, y3, color, u2, v2)
        idx = putValues(vertices, idx, x4, y4, color, u2, v)
        this.idx = idx
    }

    private fun checkBegin() {
        if (!drawing) throw IllegalStateException("SpriteBatch.begin must be called before draw.")
    }

    override fun flush() {
        if (idx == 0) return

        renderCalls++
        totalRenderCalls++
        val spritesInBatch = idx / 20
        if (spritesInBatch > maxSpritesInBatch) maxSpritesInBatch = spritesInBatch
        val count = spritesInBatch * 6

        lastTexture!!.bind()
        val mesh = this.mesh
        mesh.setVertices(vertices, 0, idx)
        mesh.indicesBuffer.position(0)
        mesh.indicesBuffer.limit(count)

        if (blendingDisabled) {
            Gdx.gl.glDisable(GL20.GL_BLEND)
        } else {
            Gdx.gl.glEnable(GL20.GL_BLEND)
            if (blendSrcFunc != -1) Gdx.gl.glBlendFunc(blendSrcFunc, blendDstFunc)
        }

        mesh.render(if (customShader != null) customShader else shader, GL20.GL_TRIANGLES, 0, count)

        idx = 0
    }

    override fun disableBlending() {
        if (blendingDisabled) return
        flush()
        blendingDisabled = true
    }

    override fun enableBlending() {
        if (!blendingDisabled) return
        flush()
        blendingDisabled = false
    }

    override fun setBlendFunction(srcFunc: Int, dstFunc: Int) {
        if (blendSrcFunc == srcFunc && blendDstFunc == dstFunc) return
        flush()
        blendSrcFunc = srcFunc
        blendDstFunc = dstFunc
    }

    override fun getBlendSrcFunc(): Int {
        return blendSrcFunc
    }

    override fun getBlendDstFunc(): Int {
        return blendDstFunc
    }

    override fun dispose() {
        mesh.dispose()
        if (ownsShader && shader != null) shader.dispose()
    }

    override fun getProjectionMatrix(): Matrix4 {
        return projectionMatrix
    }

    override fun getTransformMatrix(): Matrix4 {
        return transformMatrix
    }

    override fun setProjectionMatrix(projection: Matrix4) {
        if (drawing) flush()
        projectionMatrix.set(projection)
        if (drawing) setupMatrices()
    }

    override fun setTransformMatrix(transform: Matrix4) {
        if (drawing) flush()
        transformMatrix.set(transform)
        if (drawing) setupMatrices()
    }

    private fun setupMatrices() {
        combinedMatrix.set(projectionMatrix).mul(transformMatrix)
        if (customShader != null) {
            customShader!!.setUniformMatrix("u_projTrans", combinedMatrix)
            customShader!!.setUniformi("u_texture", 0)
        } else {
            shader!!.setUniformMatrix("u_projTrans", combinedMatrix)
            shader.setUniformi("u_texture", 0)
        }
    }

    protected fun switchTexture(texture: Texture) {
        flush()
        lastTexture = texture
        invTexWidth = 1.0f / texture.width
        invTexHeight = 1.0f / texture.height
    }

    override fun setShader(shader: ShaderProgram) {
        if (drawing) {
            flush()
            if (customShader != null)
                customShader!!.end()
            else
                this.shader!!.end()
        }
        customShader = shader
        if (drawing) {
            if (customShader != null)
                customShader!!.begin()
            else
                this.shader!!.begin()
            setupMatrices()
        }
    }

    override fun getShader(): ShaderProgram? {
        if (customShader == null) {
            return shader
        }
        return customShader
    }

    override fun isBlendingEnabled(): Boolean {
        return !blendingDisabled
    }

    override fun isDrawing(): Boolean {
        return drawing
    }

    companion object {

        @Deprecated("Do not use, this field is for testing only and is likely to be removed. Sets the {@link VertexDataType} to be\n      used when gles 3 is not available, defaults to {@link VertexDataType#VertexArray}.")
        var defaultVertexDataType = VertexDataType.VertexArray

        /**
         * Returns a new instance of the default shader used by SpriteBatch for GL2 when no shader is specified.
         */
        fun createDefaultShader(): ShaderProgram {
            val vertexShader = """
attribute vec4 ${ShaderProgram.POSITION_ATTRIBUTE};
attribute vec4 ${ShaderProgram.COLOR_ATTRIBUTE};
attribute vec2 ${ShaderProgram.TEXCOORD_ATTRIBUTE}0;
uniform mat4 u_projTrans;
varying vec4 v_color;
varying vec2 v_texCoords;

void main() {
   v_color = ${ShaderProgram.COLOR_ATTRIBUTE};
   v_color.a = v_color.a * (255.0/254.0);
   v_texCoords = ${ShaderProgram.TEXCOORD_ATTRIBUTE}0;
   gl_Position = u_projTrans * ${ShaderProgram.POSITION_ATTRIBUTE};
}
"""
            val fragmentShader = """
#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;
uniform sampler2D u_texture;

void main() {
  gl_FragColor = v_color * texture2D(u_texture, v_texCoords);
}
"""

            val shader = ShaderProgram(vertexShader, fragmentShader)
            if (!shader.isCompiled)
                throw IllegalArgumentException("Error compiling shader: " + shader.log)
            return shader
        }
    }
}