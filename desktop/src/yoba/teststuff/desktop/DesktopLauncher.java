package yoba.teststuff.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import yoba.teststuff.BatchExample;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.useHDPI = true;
//        new LwjglApplication(new MaskExample(), config);
        new LwjglApplication(new BatchExample(), config);
    }
}
